 <html>
<header><h1>Web Client Application</h1></header>
<header><h5>0 in Body Temperature indicates the temperature equipment has not been put in a person's mouth.</h5></header>
<header><h5>For accurate measurement, a person needs to put a temperature sensor to his/her mouth.</h5></header>
<Body>

<button onClick="location.href = 'http://sensornetworks.engr.uga.edu/sp14/kykanji/sort_by_survivors.php'">Sort by Survivors detected</button>
<button onClick="location.href = 'http://sensornetworks.engr.uga.edu/sp14/kykanji/sort_by_health.php'">Sort by Health</button>
<button onClick="location.href = 'http://sensornetworks.engr.uga.edu/sp14/kykanji/web_client.php'">Merged Data</button>
<form name="input" action="http://sensornetworks.engr.uga.edu/sp14/kykanji/update_request.php" method="get">
Remote Description: <input type="text" name="remote_description">
<input type="submit" value="Submit">
</form>

<?php
/*
*This PHP is a User Client Program that displays the contents in the Server table 
*in a form of an array so that users can visually monitor the data stored in the server.
*
*/

// Create connection
 $con=mysqli_connect("localhost","snadmin","snadmin*","sensornetworks");
 //$con=mysqli_connect("localhost","root","","my_db");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

echo "Connected to SQL";
//echo "<br>";

//====================================================================================================
//retrieve data from SQL server and display them in a user friendly manner. (Graphical Representation)
//====================================================================================================

$data=mysqli_query($con,"SELECT * FROM sp14_kykanji_testTable ORDER BY detected DESC");
//put the grabbed data into "$info" array
//Print "<table border cellpadding=3>"; 
echo "<table border='1'>
<tr>
<th>ID</th>
<th>Radio Address:</th>
<th>Air Temperature</th>
<th>Body Temperature</th>
<th>Vitality</th>
<th>Location</th>
<th>Life Detected</th>
<th>Health</th>
</tr>";
//now print out the contents of the array
while($info=mysqli_fetch_assoc($data)){
	echo "<tr>";
	echo "<td>" . $info['id'] . "</td>";
	echo "<td>" . $info['remote_address'] . "</td>";
	echo "<td>" . $info['temp_air'] . "</td>";
	echo "<td>" . $info['temp_body'] . "</td>";
	echo "<td>" . $info['force_val'] . "</td>";
	echo "<td>" . $info['location'] . "</td>";
	echo "<td>" . $info['detected'] . "</td>";
	echo "<td>" . $info['health'] . "</td>";
	
	echo "</tr>";
/*
	Print "<tr>";
	Print "<th>ID:</th> <td>".$info['id'] . " </td></tr>"; 
	Print "<th>Radio Address:</th> <td>".$info['remote_address'] . " </td></tr>";
	Print "<th>Location:</th> <td>".$info['location'] . " </td></tr>";
	Print "<th>Air Temperature:</th> <td>".$info['temp_air'] . " </td></tr>";
	Print "<th>Body Temperature</th> <td>".$info['temp_body'] . " </td></tr>";
	Print "<th>Vitality:</th> <td>".$info['force_val'] . " </td></tr>";
*/
	}
Print "</table>"; 

//close connection after printing the array
mysqli_close($con);
?>
</Body>
</html>
