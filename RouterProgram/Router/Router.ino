#include <SoftwareSerial.h>
#include <XBee.h>

SoftwareSerial ss(2,3);//Software Serial object
uint8_t buffer[72];
float tempAir;
float tempBody;
int forceSensor;
int forceReading=2;
int reading0=0;
int reading1=1;
int health=0;
int status_LED=13;

boolean detected=false;
unsigned long sensorTime=10000;//for delay
unsigned long sensorLast=0;
XBee xbee=XBee();//Xbee object
XBeeResponse response = XBeeResponse();
  // create reusable response objects for responses we expect to handle 
ZBRxResponse rx = ZBRxResponse();

void setup(){
  pinMode(status_LED,OUTPUT);
  Serial.begin(9600);
  ss.begin(9600);
  xbee.setSerial(ss);  
}
void receiveData(){
  xbee.readPacket();
  if (xbee.getResponse().isAvailable()) {
   //if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
    //xbee.getResponse().getZBRxResponse(rx);
    Serial.println(rx.getData(rx.getDataOffset()));
    for(int i=0;i<rx.getDataLength();i++){
      Serial.print("Received a packet. Data=");
      Serial.println(rx.getData(i),DEC);
    }
    Serial.println();
    digitalWrite(status_LED,rx.getData(rx.getDataOffset()));
    //rx.getData(0);
  }else{
    //do nothing
    Serial.println("No packets received...");
  }
   
}
void sendData(){  
  //int reading =analogRead(A0);//assuming a sensor is connected to A0
  //buffer[0]=tempAir & 0xFF;//grab the first byte
  //buffer[1]=(tempAir >> 8) & 0xFF; //grab the second byte
  buffer[0]=tempAir;//grab the first byte
  buffer[1]=tempBody;
  buffer[2]=forceSensor;
  buffer[3]=detected;
  buffer[4]=health;
  // SH + SL Address of receiving XBee
  XBeeAddress64 addr64 = XBeeAddress64(0,0);
  ZBTxRequest zbTx = ZBTxRequest(addr64, buffer, 8);
  xbee.send(zbTx);//send data
}
void force(){
 forceSensor=analogRead(forceReading); 
}
void temp(){
 float tempAIR=analogRead(reading0);
 float tempHUMAN=analogRead(reading1);
 float volt1 = tempAIR*5.0;
 float volt2 = tempHUMAN*5.0;
 volt1=volt1/1024.0f;
 volt2=volt2/1024.0f;
 tempAir=(volt1-0.5)*100.0f;
 tempBody=(volt2-0.5)*100.0f;
}
void loop(){
  if(millis()-sensorLast> sensorTime){
    Serial.println("TEST");
    sensorLast = millis();
    xbee.readPacket();
    temp();
    force();
    if(tempBody>tempAir)
      tempBody=tempBody;
    else
      tempBody=0;
    if(forceSensor>0)
     detected=true; 
    else
     detected=false;
    if(forceSensor>200.0&&tempBody>=34.0f)
     health=2;
    else if (forceSensor>100&&tempBody>=31.0f)
     health=1; 
    else 
     health=0; 
    sendData();
  }
  receiveData();
}

