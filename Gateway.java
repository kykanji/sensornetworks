/*
 * This program is a gateway program which receives a sensor data from XBee router and upload the data onto
 * a web server database through internet. 
 */
package edu.uga.engr.sensornetwork.kykanji_gateway;

import java.net.URLEncoder;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.rapplogic.xbee.XBeePin;
import com.rapplogic.xbee.api.ApiId;
import com.rapplogic.xbee.api.RemoteAtRequest;
import com.rapplogic.xbee.api.RemoteAtResponse;
import com.rapplogic.xbee.api.XBee;
import com.rapplogic.xbee.api.XBeeAddress16;
import com.rapplogic.xbee.api.XBeeAddress64;
import com.rapplogic.xbee.api.XBeeConfiguration;
import com.rapplogic.xbee.api.XBeePacket;
import com.rapplogic.xbee.api.XBeeResponse;
import com.rapplogic.xbee.api.wpan.TxRequest16;
import com.rapplogic.xbee.api.wpan.TxRequest64;
import com.rapplogic.xbee.api.wpan.TxStatusResponse;
import com.rapplogic.xbee.api.zigbee.ZNetRxResponse;
import com.rapplogic.xbee.api.zigbee.ZNetTxRequest;
import com.rapplogic.xbee.api.zigbee.ZNetTxStatusResponse;

public class Gateway {

	public Gateway(){
		new XBeeConfiguration().withStartupChecks(false);
		XBee xbee=new XBee();
		int currentLight=0;
		int previousLight=0;
		int counter=0;
		try{
			xbee.open("COM4", 9600);

			while(true){
				try{
					
					XBeeResponse response = xbee.getResponse();
					if(response.getApiId()!=ApiId.ZNET_EXPLICIT_RX_RESPONSE){
						ZNetRxResponse rx = (ZNetRxResponse)response;
						System.out.println("Got a packet.");
						System.out.println(rx.getRemoteAddress64());
						int []data=rx.getData();
						
						//int reading=data[0]|(data[1]<<8);//unpacking data: actual reading
						float reading0=data[0];
						float reading1=data[1];
						int reading2=data[2];
						int detected=data[3];
						int health=data[4];
						System.out.println("tempAir="+reading0+" tempBody="+reading1);
						System.out.println("forceSensor= "+reading2+" detected="+detected+" health="+health);
						
						//http://localhost/upload_data.php?
						/*
						String httpResponse = Request
								.Get("http://localhost/upload_data.php?"+"password=penguin"+"&"+
										"temp_air="+reading0+"&"+"temp_body="+reading1+"&"+"force="+reading2+"&"+"GPS=v"+"&"+
										"detected="+detected+"&"+"health="+health+"&"+
										"radio_address="+URLEncoder.encode(rx.getRemoteAddress64().toString(), "UTF-8"))
										.execute().returnContent().asString();
						*///for server request
						String httpResponse = Request
								.Get("http://sensornetworks.engr.uga.edu/sp14/kykanji/upload_data.php?"+"password=penguin"+"&"+
										"temp_air="+reading0+"&"+"temp_body="+reading1+"&"+"force="+reading2+"&"+"GPS=v"+"&"+
								 		"detected="+detected+"&"+"health="+health+"&"+
										"radio_address="+URLEncoder.encode(rx.getRemoteAddress64().toString(), "UTF-8"))
										.execute().returnContent().asString();
						System.out.println(httpResponse);
					}
					else{
						System.out.println("else statement;");
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				String httpResponse = Request
						.Get("http://sensornetworks.engr.uga.edu/sp14/kykanji/getMoteData.php?"+"password=penguin")
						.execute().returnContent().asString();
				System.out.println(httpResponse);
				//Thread.sleep(1000);
				//=================================JSON=====================================================
				
				JsonParser parser = new JsonParser();
				JsonElement f = parser.parse(httpResponse);
				JsonObject j=f.getAsJsonObject();
				System.out.println(j.get("success").getAsBoolean());
				JsonArray motes=j.get("data").getAsJsonArray();//now we have json array
				for(int i=0;i<motes.size();i++){
					JsonObject obj=motes.get(i).getAsJsonObject();
					String address_J=obj.get("remote_address").getAsString();//the last .get("___") is what you want to get
					System.out.println("address_J="+address_J); 
					
					//===============PARSE Radio_address====================
					String []parsed_address = new String[8];
					//int [] intParsed_address=new int[8];
					parsed_address[0]=address_J.substring(2, 4);
					parsed_address[1]=address_J.substring(7, 9);
					parsed_address[2]=address_J.substring(12, 14);
					parsed_address[3]=address_J.substring(17, 19);
					parsed_address[4]=address_J.substring(22, 24);
					parsed_address[5]=address_J.substring(27, 29);
					parsed_address[6]=address_J.substring(32, 34);
					parsed_address[7]=address_J.substring(37, 39);
					String addr_concat = parsed_address[0]+" ";
					for(int l=1;l<7;l++){
						addr_concat+=parsed_address[l]+" ";
					}
					//==================send tx=============================
					addr_concat+=parsed_address[7];
					JsonElement light_status=obj.get("rescue_light");
					System.out.println("rescue_light"+light_status);
					System.out.println("addr_concat="+addr_concat);
					int[] payload = new int[1];
					currentLight=light_status.getAsInt();

					if(currentLight!=previousLight && currentLight==1){
						System.out.println("TX sending...");
						//send tx packet 
						payload[0]=1;//turn on the light
						XBeeAddress64 destination64 = new XBeeAddress64(addr_concat);
						ZNetTxRequest tx2=new ZNetTxRequest(destination64,payload);
						//TxRequest64 tx2 = new TxRequest64(destination64, payload);
						xbee.sendAsynchronous(tx2);
						previousLight=currentLight;
					}
					else if(currentLight!=previousLight && currentLight==0){//if light_status==0
						System.out.println("TX not sending");
						payload[0]=0;//dim the light 
						XBeeAddress64 destination64 = new XBeeAddress64(addr_concat);
						ZNetTxRequest tx2=new ZNetTxRequest(destination64,payload);
						//TxRequest64 tx2 = new TxRequest64(destination64, payload);
						xbee.sendAsynchronous(tx2);
						previousLight=currentLight;
					}
					else{}//do nothing
					/*
					
					for(int k=0;k<8;k++){
						System.out.println("array="+parsed_address[k]); 
						intParsed_address[k]=Integer.parseInt(parsed_address[k]);
					}	
					JsonElement mailbox=obj.get("mailbox");
					if(!mailbox.isJsonNull()){
						//XBeeAddress64 addr=new XBeeAddress64(arg0);
						String message=mailbox.getAsString();
						System.out.println(message);
					}
					int light_status=motes.get(i).getAsInt();
					System.out.println(light_status);
					*/
				}
				
			}
		}catch(NullPointerException e){
			e.printStackTrace();
		}catch(JsonSyntaxException e){	
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("first try brace");
		}
		
		finally{
			if(xbee.isConnected()){
				xbee.close();
			}
		}
	
		}
	
	public static void main(String []args)throws Exception{
		new Gateway();
	}

}
